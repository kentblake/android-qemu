# android-qemu

simple android qemu setup to use a andoid vm on linux

# Setup
* run: `create_memory.sh`
* download latest android x86 image from here: https://www.android-x86.org/
* temporary add `-cdrom android-x86_64-9.0-r2.iso` to run.sh
* run: `run.sh` and install android (all options yes or empty) 
* remove `-cdrom android-x86_64-9.0-r2.iso` from run.sh

# Keyboard layout
* change the `-k` parameter in run.sh
* change keyboard layout in android settings

# GPU accelleration
Add to run.sh
```
-device virtio-vga,virgl=on
-display gtk,gl=on
```
Warning: doesn't work with nvidia proprietary
